Visca
=====

This library can be used to control a Visca over IP enabled PTZ camera.

Not all commands and responses are yet implemented (responses are not
implemented at all at the moment), feel free to create a merge request to add
more.

Usage
-----

```java
var client = new ViscaClient("192.168.1.234", 5678); // Intantiate
client.send(Preset.recall(1));                       // Recall Preset 1
client.close();                                      // Disconnect
```

Links
-----

- [Releases](https://gitlab.com/lightingcontrol/experiments/visca/-/releases)
- [Maven Packages](https://gitlab.com/lightingcontrol/repository/-/packages)

Release
-------

```sh
mvn clean deploy -P release
```
