package io.gitlab.lightingcontrol.visca.command.util;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ByteParamTest {

    public static Stream<Arguments> shouldConvert() {
        return Stream.of(
                Arguments.of(new ByteParam("test", 1, 10), 1, (byte) 1),
                Arguments.of(new ByteParam("test", 1, 10), 2, (byte) 2),
                Arguments.of(new ByteParam("test", 1, 10), 3, (byte) 3),
                Arguments.of(new ByteParam("test", 1, 10), 4, (byte) 4),
                Arguments.of(new ByteParam("test", 1, 10), 5, (byte) 5),
                Arguments.of(new ByteParam("test", 1, 10), 6, (byte) 6),
                Arguments.of(new ByteParam("test", 1, 10), 7, (byte) 7),
                Arguments.of(new ByteParam("test", 1, 10), 8, (byte) 8),
                Arguments.of(new ByteParam("test", 1, 10), 9, (byte) 9),
                Arguments.of(new ByteParam("test", 1, 10), 10, (byte) 10),
                Arguments.of(new ByteParam("test", 1, 239), 239, (byte) -17)
        );
    }

    public static Stream<Arguments> shouldThrowOnCreation() {
        return Stream.of(
                Arguments.of(0, 240, "Parameter max needs to be within 0…239"),
                Arguments.of(-1, 240, "Parameter min needs to be within 0…239"),
                Arguments.of(-1, 239, "Parameter min needs to be within 0…239"),
                Arguments.of(4, 3, "Parameter min cannot be greater than max")
        );
    }

    public static Stream<Arguments> shouldThrowWithMessage() {
        return Stream.of(
                Arguments.of(new ByteParam("test", 1, 10), 0, "Invalid value for test (needs to be within 1 and 10)"),
                Arguments.of(new ByteParam("test", 1, 10), 11, "Invalid value for test (needs to be within 1 and 10)"),
                Arguments.of(new ByteParam("test", 0, 0), -1, "Invalid value for test (needs to be within 0 and 0)")
        );
    }

    @MethodSource
    @ParameterizedTest
    void shouldConvert(ByteParam param, int value, byte expectedResult) {
        assertThat(param.toByte(value), is(expectedResult));
    }

    @MethodSource
    @ParameterizedTest
    void shouldThrowOnCreation(int min, int max, String expectedMessage) {
        var exception = assertThrows(IllegalArgumentException.class, () -> new ByteParam("test", min, max));

        assertThat(exception.getMessage(), is(expectedMessage));
    }

    @MethodSource
    @ParameterizedTest
    void shouldThrowWithMessage(ByteParam param, int value, String expectedMessage) {
        var exception = assertThrows(IllegalArgumentException.class, () -> param.toByte(value));

        assertThat(exception.getMessage(), is(expectedMessage));
    }

}