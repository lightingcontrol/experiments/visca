package io.gitlab.lightingcontrol.visca.command.util;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class MultiByteParamTest {

    public static Stream<Arguments> shouldConvert() {
        return Stream.of(
                Arguments.of(new MultiByteParam("test", 2, 0, 0), 0, new byte[]{(byte) 0, (byte) 0}),
                Arguments.of(new MultiByteParam("test", 2, 1, 1), 1, new byte[]{(byte) 0, (byte) 1}),
                Arguments.of(new MultiByteParam("test", 2, 2, 2), 2, new byte[]{(byte) 0, (byte) 2}),
                Arguments.of(new MultiByteParam("test", 2, 3, 3), 3, new byte[]{(byte) 0, (byte) 3}),
                Arguments.of(new MultiByteParam("test", 2, 4, 4), 4, new byte[]{(byte) 0, (byte) 4}),
                Arguments.of(new MultiByteParam("test", 2, 5, 5), 5, new byte[]{(byte) 0, (byte) 5}),
                Arguments.of(new MultiByteParam("test", 2, 6, 6), 6, new byte[]{(byte) 0, (byte) 6}),
                Arguments.of(new MultiByteParam("test", 2, 7, 7), 7, new byte[]{(byte) 0, (byte) 7}),
                Arguments.of(new MultiByteParam("test", 2, 8, 8), 8, new byte[]{(byte) 0, (byte) 8}),
                Arguments.of(new MultiByteParam("test", 2, 9, 9), 9, new byte[]{(byte) 0, (byte) 9}),
                Arguments.of(new MultiByteParam("test", 2, 10, 10), 10, new byte[]{(byte) 0, (byte) 10}),
                Arguments.of(new MultiByteParam("test", 2, 11, 11), 11, new byte[]{(byte) 0, (byte) 11}),
                Arguments.of(new MultiByteParam("test", 2, 12, 12), 12, new byte[]{(byte) 0, (byte) 12}),
                Arguments.of(new MultiByteParam("test", 2, 13, 13), 13, new byte[]{(byte) 0, (byte) 13}),
                Arguments.of(new MultiByteParam("test", 2, 14, 14), 14, new byte[]{(byte) 0, (byte) 14}),
                Arguments.of(new MultiByteParam("test", 2, 15, 15), 15, new byte[]{(byte) 0, (byte) 15}),
                Arguments.of(new MultiByteParam("test", 2, 16, 16), 16, new byte[]{(byte) 1, (byte) 0}),
                Arguments.of(new MultiByteParam("test", 2, 17, 17), 17, new byte[]{(byte) 1, (byte) 1}),
                Arguments.of(new MultiByteParam("test", 2, 18, 18), 18, new byte[]{(byte) 1, (byte) 2})
        );
    }

    @MethodSource
    @ParameterizedTest
    void shouldConvert(MultiByteParam param, int value, byte[] expectedResult) {
        assertThat(param.toByte(value), is(expectedResult));
    }

}