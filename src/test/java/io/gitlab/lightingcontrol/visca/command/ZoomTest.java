package io.gitlab.lightingcontrol.visca.command;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.HexFormat;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class ZoomTest {

    public static Stream<Arguments> shouldConvertTele() {
        return Stream.of(
                Arguments.of(0, HexFormat.of().parseHex("8101040720FF")),
                Arguments.of(7, HexFormat.of().parseHex("8101040727FF"))
        );
    }

    public static Stream<Arguments> shouldConvertWide() {
        return Stream.of(
                Arguments.of(0, HexFormat.of().parseHex("8101040730FF")),
                Arguments.of(7, HexFormat.of().parseHex("8101040737FF"))
        );
    }

    public static Stream<Arguments> shouldConvertDirect() {
        return Stream.of(
                Arguments.of(0, HexFormat.of().parseHex("8101044700000000FF")),
                Arguments.of(16384, HexFormat.of().parseHex("8101044704000000FF"))
        );
    }

    @Test
    void shouldConvertStop() {
        assertThat(Zoom.stop(), is(HexFormat.of().parseHex("8101040700FF")));
    }

    @Test
    void shouldConvertTeleStandard() {
        assertThat(Zoom.tele(), is(HexFormat.of().parseHex("8101040702FF")));
    }

    @Test
    void shouldConvertWideStandard() {
        assertThat(Zoom.wide(), is(HexFormat.of().parseHex("8101040703FF")));
    }

    @MethodSource
    @ParameterizedTest
    void shouldConvertTele(int zoomnSpeed, byte[] expectedResult) {
        assertThat(Zoom.tele(zoomnSpeed), is(expectedResult));
    }

    @MethodSource
    @ParameterizedTest
    void shouldConvertWide(int zoomnSpeed, byte[] expectedResult) {
        assertThat(Zoom.wide(zoomnSpeed), is(expectedResult));
    }

    @MethodSource
    @ParameterizedTest
    void shouldConvertDirect(int zoomn, byte[] expectedResult) {
        assertThat(Zoom.direct(zoomn), is(expectedResult));
    }

}