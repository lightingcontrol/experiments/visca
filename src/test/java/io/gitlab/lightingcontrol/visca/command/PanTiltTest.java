package io.gitlab.lightingcontrol.visca.command;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.HexFormat;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class PanTiltTest {

    public static Stream<Arguments> shouldConvertUp() {
        return Stream.of(
                Arguments.of(1, 1, HexFormat.of().parseHex("8101060101010301FF")),
                Arguments.of(24, 20, HexFormat.of().parseHex("8101060118140301FF"))
        );
    }

    public static Stream<Arguments> shouldConvertDown() {
        return Stream.of(
                Arguments.of(1, 1, HexFormat.of().parseHex("8101060101010302FF")),
                Arguments.of(24, 20, HexFormat.of().parseHex("8101060118140302FF"))
        );
    }

    public static Stream<Arguments> shouldConvertLeft() {
        return Stream.of(
                Arguments.of(1, 1, HexFormat.of().parseHex("8101060101010103FF")),
                Arguments.of(24, 20, HexFormat.of().parseHex("8101060118140103FF"))
        );
    }

    public static Stream<Arguments> shouldConvertRight() {
        return Stream.of(
                Arguments.of(1, 1, HexFormat.of().parseHex("8101060101010203FF")),
                Arguments.of(24, 20, HexFormat.of().parseHex("8101060118140203FF"))
        );
    }

    public static Stream<Arguments> shouldConvertUpRight() {
        return Stream.of(
                Arguments.of(1, 1, HexFormat.of().parseHex("8101060101010201FF")),
                Arguments.of(24, 20, HexFormat.of().parseHex("8101060118140201FF"))
        );
    }

    public static Stream<Arguments> shouldConvertUpLeft() {
        return Stream.of(
                Arguments.of(1, 1, HexFormat.of().parseHex("8101060101010101FF")),
                Arguments.of(24, 20, HexFormat.of().parseHex("8101060118140101FF"))
        );
    }

    public static Stream<Arguments> shouldConvertDownRight() {
        return Stream.of(
                Arguments.of(1, 1, HexFormat.of().parseHex("8101060101010202FF")),
                Arguments.of(24, 20, HexFormat.of().parseHex("8101060118140202FF"))
        );
    }

    public static Stream<Arguments> shouldConvertDownLeft() {
        return Stream.of(
                Arguments.of(1, 1, HexFormat.of().parseHex("8101060101010102FF")),
                Arguments.of(24, 20, HexFormat.of().parseHex("8101060118140102FF"))
        );
    }

    public static Stream<Arguments> shouldConvertStop() {
        return Stream.of(
                Arguments.of(1, 1, HexFormat.of().parseHex("8101060101010303FF")),
                Arguments.of(24, 20, HexFormat.of().parseHex("8101060118140303FF"))
        );
    }

    @MethodSource
    @ParameterizedTest
    void shouldConvertUp(int panSpeed, int tiltSpeed, byte[] expectedResult) {
        assertThat(PanTilt.up(panSpeed, tiltSpeed), is(expectedResult));
    }

    @MethodSource
    @ParameterizedTest
    void shouldConvertDown(int panSpeed, int tiltSpeed, byte[] expectedResult) {
        assertThat(PanTilt.down(panSpeed, tiltSpeed), is(expectedResult));
    }

    @MethodSource
    @ParameterizedTest
    void shouldConvertLeft(int panSpeed, int tiltSpeed, byte[] expectedResult) {
        assertThat(PanTilt.left(panSpeed, tiltSpeed), is(expectedResult));
    }

    @MethodSource
    @ParameterizedTest
    void shouldConvertRight(int panSpeed, int tiltSpeed, byte[] expectedResult) {
        assertThat(PanTilt.right(panSpeed, tiltSpeed), is(expectedResult));
    }

    @MethodSource
    @ParameterizedTest
    void shouldConvertUpRight(int panSpeed, int tiltSpeed, byte[] expectedResult) {
        assertThat(PanTilt.upRight(panSpeed, tiltSpeed), is(expectedResult));
    }

    @MethodSource
    @ParameterizedTest
    void shouldConvertUpLeft(int panSpeed, int tiltSpeed, byte[] expectedResult) {
        assertThat(PanTilt.upLeft(panSpeed, tiltSpeed), is(expectedResult));
    }

    @MethodSource
    @ParameterizedTest
    void shouldConvertDownRight(int panSpeed, int tiltSpeed, byte[] expectedResult) {
        assertThat(PanTilt.downRight(panSpeed, tiltSpeed), is(expectedResult));
    }

    @MethodSource
    @ParameterizedTest
    void shouldConvertDownLeft(int panSpeed, int tiltSpeed, byte[] expectedResult) {
        assertThat(PanTilt.downLeft(panSpeed, tiltSpeed), is(expectedResult));
    }

    @MethodSource
    @ParameterizedTest
    void shouldConvertStop(int panSpeed, int tiltSpeed, byte[] expectedResult) {
        assertThat(PanTilt.stop(panSpeed, tiltSpeed), is(expectedResult));
    }

    @Test
    void shouldConvertStopWithoutParameters() {
        assertThat(PanTilt.stop(), is(HexFormat.of().parseHex("8101060118140303FF")));
    }

    @Test
    void shouldConvertHome() {
        assertThat(PanTilt.home(), is(HexFormat.of().parseHex("81010604FF")));
    }

    @Test
    void shouldConvertReset() {
        assertThat(PanTilt.reset(), is(HexFormat.of().parseHex("81010605FF")));
    }
}