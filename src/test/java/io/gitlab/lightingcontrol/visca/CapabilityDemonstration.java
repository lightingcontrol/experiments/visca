package io.gitlab.lightingcontrol.visca;

import io.gitlab.lightingcontrol.visca.command.PanTilt;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class CapabilityDemonstration {
    private final static Logger LOG = Logger.getLogger(CapabilityDemonstration.class.getName());
    private final static long SLEEP = 5;

    public static void main(String[] args) throws IOException {

        if (args.length != 2) {
            LOG.warning("Usage:\n  ManualTest <ip/host> <port>");
            System.exit(1);
        }

        try (var client = new ViscaClient(args[0], Integer.parseInt(args[1]))) {
            new PanTiltTest(1).run(client);
            new PanTiltTest(7).run(client);
        }


    }

    interface Testcase {
        void run(ViscaClient client);
    }

    static final class PanTiltTest implements Testcase {

        private final int speed;

        public PanTiltTest(int speed) {
            this.speed = speed;
        }

        @Override
        public void run(ViscaClient client) {
            try {
                LOG.info("Moving left (speed: " + speed + ")");
                client.send(PanTilt.left(speed, speed));
                TimeUnit.SECONDS.sleep(SLEEP);
                LOG.info("Moving up (speed: " + speed + ")");
                client.send(PanTilt.up(speed, speed));
                TimeUnit.SECONDS.sleep(SLEEP);
                LOG.info("Moving right (speed: " + speed + ")");
                client.send(PanTilt.right(speed, speed));
                TimeUnit.SECONDS.sleep(SLEEP);
                LOG.info("Moving down (speed: " + speed + ")");
                client.send(PanTilt.down(speed, speed));
                TimeUnit.SECONDS.sleep(SLEEP);
                LOG.info("UpRight (speed: " + speed + ")");
                client.send(PanTilt.upRight(speed, speed));
                TimeUnit.SECONDS.sleep(SLEEP);
                LOG.info("DownRight (speed: " + speed + ")");
                client.send(PanTilt.downRight(speed, speed));
                TimeUnit.SECONDS.sleep(SLEEP);
                LOG.info("DownLeft (speed: " + speed + ")");
                client.send(PanTilt.downLeft(speed, speed));
                TimeUnit.SECONDS.sleep(SLEEP);
                LOG.info("UpLeft (speed: " + speed + ")");
                client.send(PanTilt.upLeft(speed, speed));
                TimeUnit.SECONDS.sleep(SLEEP);
                LOG.info("Stop (speed: " + speed + ")");
                client.send(PanTilt.stop(speed, speed));
                TimeUnit.SECONDS.sleep(SLEEP);
                LOG.info("Home");
                client.send(PanTilt.home());
            } catch (IOException | InterruptedException ex) {
                LOG.severe(ex.getMessage());
            }
        }
    }

}
