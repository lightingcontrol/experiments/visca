package io.gitlab.lightingcontrol.visca.command;

import io.gitlab.lightingcontrol.visca.command.util.ByteParam;
import io.gitlab.lightingcontrol.visca.command.util.MultiByteParam;

import java.nio.ByteBuffer;
import java.util.HexFormat;

public class Preset {
    private static final ByteParam PARAM_PRESET = new ByteParam("preset", 0, 127);
    private static final ByteParam PARAM_SPEED = new ByteParam("speed", 0, 18);
    private static final MultiByteParam PARAM_AXIS = new MultiByteParam("speed", 4, 0, 4095);

    private Preset() {
    }

    // memoryNumber should be 0-127
    public static byte[] reset(int preset) {
        var buffer = ByteBuffer.allocate(7);
        buffer.put(HexFormat.of().parseHex("8101043F00"));
        buffer.put(PARAM_PRESET.toByte(preset));
        buffer.put(HexFormat.of().parseHex("FF"));
        return buffer.array();
    }

    // memoryNumber should be 0-127
    public static byte[] set(int preset) {
        var buffer = ByteBuffer.allocate(7);
        buffer.put(HexFormat.of().parseHex("8101043F01"));
        buffer.put(PARAM_PRESET.toByte(preset));
        buffer.put(HexFormat.of().parseHex("FF"));
        return buffer.array();
    }

    // memoryNumber should be 0-127
    public static byte[] recall(int preset) {
        var buffer = ByteBuffer.allocate(7);
        buffer.put(HexFormat.of().parseHex("8101043F02"));
        buffer.put(PARAM_PRESET.toByte(preset));
        buffer.put(HexFormat.of().parseHex("FF"));
        return buffer.array();
    }

    // Speed should be 1-18
    public static byte[] presetRecallSpeed(int speed) {
        var buffer = ByteBuffer.allocate(7);
        buffer.put(HexFormat.of().parseHex("8101043F00"));
        buffer.put(PARAM_SPEED.toByte(speed));
        buffer.put(HexFormat.of().parseHex("FF"));
        return buffer.array();
    }

    public static byte[] limitSet(boolean upRight, int pan, int tilt) {
        var buffer = ByteBuffer.allocate(15);
        buffer.put(HexFormat.of().parseHex("8101060700"));
        buffer.put(upRight ? (byte) 1 : (byte) 0);
        buffer.put(PARAM_AXIS.toByte(pan));
        buffer.put(PARAM_AXIS.toByte(tilt));
        buffer.put(HexFormat.of().parseHex("FF"));
        return buffer.array();
    }

    public static byte[] limitClear(boolean upRight) {
        return HexFormat.of().parseHex(
                upRight
                        ? "810106070101070F0F0F070F0F0FFF"
                        : "810106070100070F0F0F070F0F0FFF");
    }

    public static byte[] motionSync(boolean on) {
        return HexFormat.of().parseHex(
                on
                        ? "810A111302FF"
                        : "810A111303FF");
    }

}