package io.gitlab.lightingcontrol.visca.command;

import io.gitlab.lightingcontrol.visca.command.util.ByteParam;
import io.gitlab.lightingcontrol.visca.command.util.MultiByteParam;

import java.nio.ByteBuffer;
import java.util.HexFormat;

public class Zoom {

    private static final ByteParam PARAM_ZOOM_SPEED = new ByteParam("zoomSpeed", 0, 7);
    private static final MultiByteParam PARAM_ZOOM = new MultiByteParam("zoom", 4, 0, 16384);

    private Zoom() {
    }

    public static byte[] stop() {
        return HexFormat.of().parseHex("8101040700FF");
    }

    public static byte[] tele() {
        return HexFormat.of().parseHex("8101040702FF");
    }

    public static byte[] wide() {
        return HexFormat.of().parseHex("8101040703FF");
    }

    // zoomSpeed should be 0…7
    public static byte[] tele(int zoomSpeed) {
        var buffer = ByteBuffer.allocate(6);
        buffer.put(HexFormat.of().parseHex("81010407"));
        buffer.put((byte) (PARAM_ZOOM_SPEED.toByte(zoomSpeed) | (byte) 0x20));
        buffer.put(HexFormat.of().parseHex("FF"));
        return buffer.array();
    }

    // zoomSpeed should be 0…7
    public static byte[] wide(int zoomSpeed) {
        var buffer = ByteBuffer.allocate(6);
        buffer.put(HexFormat.of().parseHex("81010407"));
        buffer.put((byte) (PARAM_ZOOM_SPEED.toByte(zoomSpeed) | (byte) 0x30));
        buffer.put(HexFormat.of().parseHex("FF"));
        return buffer.array();
    }

    public static byte[] direct(int zoom) {
        var buffer = ByteBuffer.allocate(9);
        buffer.put(HexFormat.of().parseHex("81010447"));
        buffer.put(PARAM_ZOOM.toByte(zoom));
        buffer.put(HexFormat.of().parseHex("FF"));
        return buffer.array();
    }

}
