package io.gitlab.lightingcontrol.visca.command;

import io.gitlab.lightingcontrol.visca.command.util.ByteParam;

import java.nio.ByteBuffer;
import java.util.HexFormat;

public class Image {

    private static final ByteParam GAIN_PARAM = new ByteParam("gain", 0, 14);

    private Image() {
    }

    public static byte[] flipHorizontal(boolean enabled) {
        return HexFormat.of().parseHex(enabled ? "8101046102FF" : "8101046103FF");
    }

    public static byte[] flipVertical(boolean enabled) {
        return HexFormat.of().parseHex(enabled ? "8101046602FF" : "8101046603FF");
    }

    // gain should be 0-14 (60%-200%)
    public static byte[] colorGain(int gain) {
        var buffer = ByteBuffer.allocate(9);
        buffer.put(HexFormat.of().parseHex("81010449000000"));
        buffer.put(GAIN_PARAM.toByte(gain));
        buffer.put(HexFormat.of().parseHex("FF"));
        return buffer.array();
    }

}
