package io.gitlab.lightingcontrol.visca;

import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

public class ViscaClient implements Closeable {

    private final Socket socket;
    private final OutputStream output;

    public ViscaClient(String host, int port) throws IOException {
        this.socket = new Socket(host, port);
        this.output = socket.getOutputStream();
    }

    public void send(byte[] command) throws IOException {
        this.output.write(command);
    }

    @Override
    public void close() throws IOException {
        this.output.close();
        this.socket.close();
    }
}
