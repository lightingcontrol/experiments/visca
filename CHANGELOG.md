# Changelog

## [UNRELEASED]

### Added

- Documented links in README to the releases and maven packages & fixed example
  usage.

## [0.0]

### Added

- [ViscaClient](src/main/java/io/gitlab/lightingcontrol/visca/ViscaClient.java)
  to send VISCA commands to a PTZ camera.
- Command converters converting values for commands in the command.
